import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productpage',
  templateUrl: './productpage.component.html',
  styleUrl: './productpage.component.css'
})
export class ProductpageComponent implements OnInit {
  selectedProduct: any;

  ngOnInit() {
    // Retrieve the stored product details from localStorage
    const storedProduct = localStorage.getItem('selectedProduct');

    // Parse the JSON string to get the actual object
    this.selectedProduct = storedProduct ? JSON.parse(storedProduct) : null;
    console.log(storedProduct);
  }}
