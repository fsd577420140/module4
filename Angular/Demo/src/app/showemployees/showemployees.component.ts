import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrls: ['./showemployees.component.css']
})
export class ShowemployeesComponent {
  emailId:any;
  employee:any;

  employees: any[] = [
    { empId: 101, empName: 'Mounika', salary: 40000.00, gender: 'Female', doj: '2023-10-21', country: 'INDIA', emailId: 'akhila@gmail.com', password: 123 },
    { empId: 102, empName: 'Meghana', salary: 45000.00, gender: 'Female', doj: '2022-08-20', country: 'USA', emailId: 'manoj@gmail.com', password: 123 },
    { empId: 103, empName: 'Sony', salary: 50000.00, gender: 'Female', doj: '2021-07-18', country: 'Australia', emailId: 'siri@gmail.com', password: 123 },
    { empId: 104, empName: 'Tarun', salary: 55000.00, gender: 'Male', doj: '2020-11-16', country: 'Dubai', emailId: 'sridhar@gmail.com', password: 123 },
    { empId: 105, empName: 'Greeshma', salary: 56000.00, gender: 'Female', doj: '2021-09-23', country: 'Canada', emailId: 'abhi@gmail.com', password: 123 },
  ]

  constructor(private service:EmpService) {
    this.emailId = localStorage.getItem('emailId');
  }

  ngOnInit() {
    this.service.getAllEmployees().subscribe((data: any) => {
      this.employee = data;
      console.log(data);
    });
  }

  submit() {
    console.log(this.employees);
  }

  getEmployeeById(empId: number) {
    return this.employees.find(employee => employee.empId === empId);
  }
}
