import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'exp'
})
export class ExpPipe implements PipeTransform {

  experience:any;
  currentYear:any;
  JoiningYear:any;

  transform(value: any): any {
    this.currentYear=new Date().getFullYear();
    this.JoiningYear=new Date(value).getFullYear();
    this.experience = this.currentYear - this.JoiningYear;
    return this.experience;
  }

}
